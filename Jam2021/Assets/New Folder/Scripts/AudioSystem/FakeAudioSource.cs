using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeAudioSource : MonoBehaviour
{
    [SerializeField] public string sound;
    [SerializeField] private float soundPower;
    [SerializeField] private float soundLifeTime;
    private GameObject soundPrefab;

    private void Start()
    {
        soundPrefab = Resources.Load<GameObject>("Sound");
    }

    public void Play()
    { 
        if (soundPrefab == null)
            soundPrefab = Resources.Load<GameObject>("Sound");
        var go = Instantiate(soundPrefab, transform.position, Quaternion.identity);
        go.GetComponent<Sound>().PlaySound(sound, soundLifeTime, soundPower);
    }
}
