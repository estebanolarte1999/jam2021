using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Sound : MonoBehaviour
{
    [SerializeField] private TextMeshPro sound;
    [SerializeField] private GameObject arrow;

    private float startOpacity;

    public void PlaySound(string _sound, float lifeTime, float power)
    {
        sound.text = _sound;
        if (PlayerController.Instance != null)
        {
            if (Vector2.Distance(transform.position, PlayerController.Instance.transform.position) > power)
            {
                Destroy(gameObject);
            }
            else
            {
                startOpacity = 1f - (Vector2.Distance(transform.position, PlayerController.Instance.transform.position) / power);
            }
        }
        else
        {
            startOpacity = 1;
        }
        transform.SetParent(SoundContent.trans);

        var height = Screen.height;
        var screenx = Screen.width * Camera.main.orthographicSize / height;
        var screeny = Screen.height * Camera.main.orthographicSize / height;
        Vector2 temp = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
        Vector2 loc = new Vector2(transform.position.x, transform.position.y) - temp;

        var useArrow = false;
        if (loc.x > screenx || loc.x< -screenx ||loc.y > screeny || loc.y < -screeny)
        {
            Vector2 finalVector = Vector2.zero;

            var m = loc.y / loc.x;

            if (loc.x == 0)
            {
                finalVector = new Vector2(0, screeny * Mathf.Sign(loc.y));
            }
            else if (loc.y == 0)
            {
                finalVector = new Vector2(screenx * Mathf.Sign(loc.x), 0);
            }
            else
            {
                finalVector.y = (func1(screenx * Mathf.Sign(loc.x), m));
                finalVector.x = screenx * Mathf.Sign(loc.x);
                if (finalVector.y > screeny || finalVector.y < -screeny)
                {
                    finalVector.x = (func2(screeny * Mathf.Sign(loc.y), m));
                    finalVector.y = screeny * Mathf.Sign(loc.y);
                }
            }
            finalVector -= loc.normalized;
            finalVector += temp;

            transform.position = finalVector;
            useArrow = true;
        }
        if (useArrow)
        {
            StartCoroutine(Animation(lifeTime, loc.normalized));
        }
        else
        {
            StartCoroutine(Animation(lifeTime));
        }
    }
    private IEnumerator Animation(float lifeTime, Vector2 pos = default)
    {
        GameObject arrowGO = null;
        if (pos != Vector2.zero)
        {
            arrowGO = Instantiate(arrow, transform.position + new Vector3(pos.x,pos.y), Quaternion.identity);
            arrowGO.transform.right = new Vector3(pos.x, pos.y);
            arrowGO.transform.SetParent(SoundContent.trans);
        }
        float time = 0;
        while (time<= lifeTime)
        {
            var op = ((lifeTime - time) / lifeTime) * startOpacity;
            sound.alpha = op;
            if (arrowGO != null)
            {
                arrowGO.GetComponent<Renderer>().material.color = new Color(1, 1, 1, op);
            }
            transform.position += new Vector3(0, 0.005f);
            time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        if (arrowGO != null)
        {
            Destroy(arrowGO);
        }
        Destroy(gameObject);
    }
    private float func1(float x, float m)
    {
        return m * x;
    }
    private float func2(float x, float m)
    {
        return x / m;
    }
}
