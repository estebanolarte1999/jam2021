using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimations : MonoBehaviour
{
    [SerializeField] private RuntimeAnimatorController jackAnimator;
    [SerializeField] private RuntimeAnimatorController jennyAnimator;

    [SerializeField] private FakeAudioSource stepSound;
    [SerializeField] private float stepeDelay = 1;
    [SerializeField] private FakeAudioSource trappedScreamSound;
    [SerializeField] private float screamDelay = 3;

    private PhotonView pv;
    private Animator animator;
    private SpriteRenderer render;
    private PlayerController player;

    private bool rightDir = true;

    private IEnumerator stepSoundLoop;
    private void Start()
    {
        pv = GetComponent<PhotonView>();
        animator = GetComponent<Animator>();
        render = GetComponent<SpriteRenderer>();
        player = GetComponent<PlayerController>();
        stepSoundLoop = stepSounds();

        player.onAssingID += OnAssingID;

        if (player.ID != 0)
        {
            OnAssingID(player.ID);
        }

        if (!pv.IsMine) return;

        GetComponent<Movement>().onMovement += OnMove;
        player.OnRootedStateChange += Trapped;
    }

    private void OnAssingID(int id)
    {
        if (id == 1)
        {
            animator.runtimeAnimatorController = jackAnimator;
        }
        else
        {
            animator.runtimeAnimatorController = jennyAnimator;
        }
    }
    private void OnMove(Vector2 dir)
    {
        if (dir.x > 0)
        {
            rightDir = true;
        }
        else if( dir.x < 0)
        {
            rightDir = false;
        }
        var speed = dir.magnitude;
        pv.RPC("OnMoveRPC", RpcTarget.All, rightDir, speed*10);
    }
    private void Trapped(bool trapped)
    {
        pv.RPC("TtrappedRPC", RpcTarget.All, trapped);
        if (trapped)
        {
            pv.RPC("OnMoveRPC", RpcTarget.All, rightDir, 0f);
        }
    }
    private bool runing = false;
    [PunRPC]
    private void OnMoveRPC(bool right, float speed)
    {
        if (animator == null || animator.runtimeAnimatorController == null) return;

        if (!runing && speed !=0)
        {
            runing = true;
            StopCoroutine(stepSoundLoop);
            stepSoundLoop = stepSounds();
            StartCoroutine(stepSoundLoop);
        }
        if (speed == 0)
        {
            runing = false;
        }
        animator.SetFloat("speed", speed);
        render.flipX = !right;
    }
    private IEnumerator stepSounds()
    {
        while (runing)
        {
            stepSound.Play();
            yield return new WaitForSeconds(stepeDelay);
        }
    }
    private bool trapped = false;
    [PunRPC]
    private void TtrappedRPC(bool _trapped)
    {
        if (animator == null || animator.runtimeAnimatorController == null) return;
        trapped = _trapped;
        if (trapped)
        {
            StartCoroutine(screamLoop());
        }
        animator.SetBool("trapped", _trapped);
    }
    private IEnumerator screamLoop()
    {
        while (trapped)
        {
            trappedScreamSound.Play();
            yield return new WaitForSeconds(screamDelay);
        }
    }
}
