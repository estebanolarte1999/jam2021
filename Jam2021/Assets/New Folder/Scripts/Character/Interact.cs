using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    [HideInInspector] public PlayerController player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var other = collision.GetComponent<IInteractuable>();
        if (other != null)
            other.Interact(player);
    }
}
