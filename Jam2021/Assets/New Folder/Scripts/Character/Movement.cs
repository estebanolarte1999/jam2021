using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class Movement : MonoBehaviour
{
    [SerializeField] private float movementSpeed = 7f;
    [SerializeField] private float acceleration = 10;
    [SerializeField] private float deacceleration = 10;

    [HideInInspector] public PlayerController player;

    public Action<Vector2> onMovement;

    public bool AllowMovementInput { get; set; }
    public bool AllowMovement { get; set; }

    private Vector2 currentMovementVector;
    private Vector2 desiredMovementVector;
    private Vector2 deltaMovementVector;

    private Vector2 inputVector;

    private bool ReadInput()
    {
        if (!AllowMovementInput)
        {
            inputVector = Vector2.zero;
            return false;
        }

        inputVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (inputVector == Vector2.zero)
            return false;
        return true;  
    }

    private void DoMovement()
    {      
        if (!AllowMovement)
        {
            desiredMovementVector = Vector2.zero;
            currentMovementVector = Vector2.zero;
            deltaMovementVector = Vector2.zero;
        }
        else
        {
            float lerpSpeed;
            if (ReadInput()) 
            {
                desiredMovementVector = inputVector * movementSpeed * Time.deltaTime;
                lerpSpeed = acceleration;
            }
            else
            {
                desiredMovementVector = Vector2.zero;
                lerpSpeed = deacceleration;
            }
            currentMovementVector = Vector2.MoveTowards(currentMovementVector, desiredMovementVector, lerpSpeed * Time.deltaTime);
        }
        transform.position += new Vector3(currentMovementVector.x, currentMovementVector.y, 0);

        if(currentMovementVector != deltaMovementVector)
        {
            onMovement?.Invoke(currentMovementVector);
        }
        deltaMovementVector = currentMovementVector;       
    }

    private void FixedUpdate()
    {
        DoMovement();
    }
}
