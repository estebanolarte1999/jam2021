using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviourPunCallbacks
{
    [SerializeField] public PhotonView pv;

    [SerializeField] public Movement movement;
    [SerializeField] public Interact interact;

    public Action<int> onAssingID;

    private bool rooted;
    public bool Rooted { get => rooted; set { OnRootedStateChange?.Invoke(value); rooted = value; SyncRootLocal(value); } }

    private static int gemsSelf;
    private static int gemsOther;

    public int ID { get; private set; }

    public static PlayerController Instance;
    public static PlayerController Other;

    public Action<bool> OnRootedStateChange;
    public Action<int> OnSelfGemsChange;

    private void Awake()
    {
        movement.player = this;
        movement.AllowMovement = true;
        movement.AllowMovementInput = true;
        interact.player = this;

        if (!pv.IsMine)
        {
            Destroy(movement);
            Destroy(interact);
        }

        CheckOtherPlayer();
    }
    private void Start()
    {
        if (pv.IsMine)
        {
            CameraControl.Instance.target = transform;
        }
    }

    public void SetGemsSelf(int self)
    {
        gemsSelf = self;
        OnSelfGemsChange?.Invoke(gemsSelf);
    }

    public int GetGemsSelf()
    {
        return gemsSelf;
    }

    public void SetGemsOther(int other)
    {
        gemsOther = other;
    }

    public int GetGemsOther()
    {
        return gemsOther;
    }

    private void AssignID(int id)
    {
        ID = id;
        onAssingID?.Invoke(ID);
    }

    private void CheckOtherPlayer()
    {
        if (pv.IsMine)
            Instance = this;
        else
        {
            Other = this;
            return;
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (ID == 0)
                AssignID(1);

            pv.RPC("SyncIDs", RpcTarget.Others, ID);
        }
        else
        {
            StartCoroutine(WaitPartnerId());
        }
    }

    IEnumerator WaitPartnerId()
    {
        yield return new WaitUntil(() => Other != null && Other.ID != 0);

        if (Other.ID == 1)
            AssignID(2);
        else
            AssignID(1);

        pv.RPC("SyncIDs", RpcTarget.Others, ID);
    }

    [PunRPC]
    public void SyncIDs(int id)
    {
        AssignID(id);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        CheckOtherPlayer();
    }

    private void SyncRootLocal(bool value)
    {     
        if (pv.IsMine)
        {
            movement.AllowMovement = !value;
            movement.AllowMovementInput = !value;
            pv.RPC("SyncRooted", RpcTarget.Others, value);
        }
    }

    [PunRPC]
    public void SyncRooted(bool value)
    {
       Rooted = value;
    }
}
