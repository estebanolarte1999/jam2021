using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [HideInInspector] public Transform target;
    public static CameraControl Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        if (target!= null)
        {
            transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
        }
    }
}
