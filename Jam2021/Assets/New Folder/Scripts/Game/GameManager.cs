using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] private PhotonView pv;
    [SerializeField] private OnJoinedInstantiate OnJoinedInstantiate;
    [SerializeField] public SpawnController spawner;
    [SerializeField] private WinCondition winCondition;

    [SerializeField] private int maxLifes = 3;

    private int currentLifes;
    private int currentPoints = -1;

    public float timer;

    public Totem CurrentTotem { get; set; }

    [SerializeField] private Totem initialTotem;

    public Action OnPlayersLost;

    private IEnumerator clock;

    private bool isFailing = false;
    [SerializeField] float failDelay = 2f;

    public Action<int, int> OnTimePasses;
    public Action<int,int> OnLifesChange;
    public Action<int> OnPointsChange;
    public Action<int> OnMaxGemsChange;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        currentLifes = maxLifes;
        StartCoroutine(WaitUntilPartner());
        OnJoinedInstantiate.SpawnObjects();
        
        clock = Clock(0f);
    }

    private IEnumerator WaitUntilPartner()
    {
        yield return new WaitUntil(() => PlayerController.Instance != null && PlayerController.Other != null);

        OnLifesChange?.Invoke(currentLifes, maxLifes);

        CheckNextStage(initialTotem);
        if (PlayerController.Instance.ID == 1)
            PlayerController.Instance.transform.position = CurrentTotem.spawnA.position;
        else
            PlayerController.Instance.transform.position = CurrentTotem.spawnB.position;
    }

    private IEnumerator Clock(float _t)
    {
        var t = _t;
        while (t > 0)
        {
            t -= Time.deltaTime;
            OnTimePasses?.Invoke((int)(t / 60f), (int)(t % 60));
            yield return new WaitForEndOfFrame();
        }
        Fail();
    }

    public void InitClock(float t)
    {
        StopCoroutine(clock);
        clock = Clock(t);
        StartCoroutine(clock);
    }

    public void StopClock()
    {
        StopCoroutine(clock);
    }

    public void Win()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Fail()
    {
        if (isFailing) return;
        isFailing = true;

        StartCoroutine(FailDelay());
    }

    private IEnumerator FailDelay()
    {
        yield return new WaitForSeconds(failDelay);

        PlayerController.Instance.Rooted = false;
        if (PhotonNetwork.IsMasterClient)
            pv.RPC("LostALife", RpcTarget.All);

        isFailing = false;
    }

    private void Update()
    {
        if (PlayerController.Instance == null || PlayerController.Other == null) return;

        if(PlayerController.Instance.Rooted && PlayerController.Other.Rooted)
        {
            Fail();
        }
    }

    [PunRPC]
    public void LostALife()
    {
        currentLifes--;
        OnLifesChange?.Invoke(currentLifes, maxLifes);       
        PlayerController.Instance.SetGemsOther(0);
        PlayerController.Instance.SetGemsSelf(0);
            
        if (currentLifes == 0)
        {
            currentLifes = maxLifes;
            OnLifesChange?.Invoke(currentLifes, maxLifes);
            CurrentTotem = initialTotem;
            OnPlayersLost?.Invoke();
            CurrentTotem.StartTotem();
            currentPoints = 0;
            OnPointsChange?.Invoke(currentPoints);
            OnMaxGemsChange?.Invoke(CurrentTotem.objectivesPerPlayer);
        }
        else
        {
            CurrentTotem.StartTotem();
        }

        if (PlayerController.Instance.ID == 1)
            PlayerController.Instance.transform.position = CurrentTotem.spawnA.position;
        else if (PlayerController.Instance.ID == 2)
            PlayerController.Instance.transform.position = CurrentTotem.spawnB.position;
    }

    private void CheckNextStage(Totem totem)
    {
        if(totem != null)
        {
            CurrentTotem = totem;
            totem.ToggleCallingPlayers(true);
            totem.OnComplete += CheckNextStage;
            currentPoints++;
            OnPointsChange?.Invoke(currentPoints);
            OnMaxGemsChange?.Invoke(CurrentTotem.objectivesPerPlayer);
        }
        else
        {
            OnMaxGemsChange?.Invoke(0);
            winCondition.ToggleCallingPlayers(true);
        }
    }
}
