using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Totem : MonoBehaviour
{
    [SerializeField] private PhotonView pv;
    [SerializeField] public Transform spawnA;
    [SerializeField] public Transform spawnB;
    [SerializeField] private Totem nextTotem;
    [SerializeField] private GameObject[] doors;
    [SerializeField] private FakeAudioSource fakeAudio;
    [SerializeField] private float callingInterval = 10f;
    [SerializeField] private GameObject fireRing;

    [Header("Level configs")]
    [SerializeField] public float time;
    [SerializeField] public int objectivesPerPlayer;
    [SerializeField] public int decoysQuantity;
    [SerializeField] public int trapsQuantity;
    [SerializeField, Range(0, 1)] public float mouseProb;
    [SerializeField] public SpawneableObjects[] decoys;
    [SerializeField] public SpawneableObjects[] traps;
    [SerializeField] public SpawnPoint[] spawns;

    [Header("Manual")]
    [SerializeField] public bool manualSetUp;
    [SerializeField] SpawneableObjects[] manualObjects;

    public bool IsCalling { get; set; }

    public bool IsOngoing { get; set; }

    public bool IsComplete { get; set; }

    private int currentPlayersInRange;

    public Action<Totem> OnComplete;

    public void Start()
    {
        GameManager.Instance.OnPlayersLost += RestartTotem;
    }

    IEnumerator CallPlayers()
    {
        while (IsCalling)
        {
            fakeAudio.Play();
            yield return new WaitForSeconds(callingInterval);
        }
    }

    public void ToggleCallingPlayers(bool state)
    {
        if (state)
        {
            IsCalling = true;
            StartCoroutine(CallPlayers());
        }
        else
        {
            IsCalling = false;
        }
    }

    private void Update()
    {
        if(IsComplete)
        {
            if (!fireRing.activeSelf)
                fireRing.SetActive(true);
        }
        else
        {
            if (fireRing.activeSelf)
                fireRing.SetActive(false);
        }
    }

    public void CheckComplete()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        IsComplete = PlayerController.Instance.GetGemsSelf() >= objectivesPerPlayer && PlayerController.Instance.GetGemsOther() >= objectivesPerPlayer;
        if (!IsComplete) return;

        if (manualSetUp)
            DeleteManualElements();
        else
            GameManager.Instance.spawner.DeleteAllPreviusElements();

        OnComplete?.Invoke(nextTotem);
        OnComplete = null;
        foreach (var item in doors)
        {
            item.SetActive(false);
        }

        GameManager.Instance.StopClock();
        PlayerController.Instance.SetGemsOther(0);
        PlayerController.Instance.SetGemsSelf(0);

        pv.RPC("ForceComplete", RpcTarget.Others);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var obj = collision.GetComponent<PlayerController>();
        if (obj != null)
            currentPlayersInRange++;

        if(currentPlayersInRange >= 2)
        {
            if (!IsComplete)
            {
                if (IsOngoing)
                    CheckComplete();
                else
                {
                    IsOngoing = true;
                    ToggleCallingPlayers(false);
                    StartTotem();                  
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var obj = collision.GetComponent<PlayerController>();
        if (obj != null)
            currentPlayersInRange--;
    }

    [PunRPC]
    public void ForceComplete()
    {
        if (IsComplete) return;
        IsComplete = true;

        if (manualSetUp)
            DeleteManualElements();
        else
            GameManager.Instance.spawner.DeleteAllPreviusElements();

        OnComplete?.Invoke(nextTotem);
        OnComplete = null;
        foreach (var item in doors)
        {
            item.SetActive(false);
        }

        GameManager.Instance.StopClock();
        PlayerController.Instance.SetGemsOther(0);
        PlayerController.Instance.SetGemsSelf(0);
    }

    private void DeleteManualElements()
    {
        foreach (var item in manualObjects)
        {
            item.gameObject.SetActive(false);
        }
    }

    public void StartTotem()
    {
        GameManager.Instance.InitClock(time);
        if (manualSetUp)
        {
            foreach (var item in manualObjects)
            {
                item.gameObject.SetActive(true);
            }
        }
        GameManager.Instance.spawner.InitLevel(GameManager.Instance.CurrentTotem);
    }

    public void RestartTotem()
    {
        foreach (var item in doors)
        {
            item.SetActive(true);
        }
        IsCalling = false;
        IsComplete = false;
        IsOngoing = false;
    }
}
