using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinCondition : MonoBehaviour
{
    [SerializeField] private PhotonView pv;

    [SerializeField] private FakeAudioSource fakeAudio;
    [SerializeField] private float callingInterval = 10f;

    private bool wins = false;

    public bool IsCalling { get; set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (wins) return;
        var obj = collision.GetComponent<PlayerController>();
        if(obj != null)
        {
            wins = true;
            GameManager.Instance.Win();
            pv.RPC("ForceWin", RpcTarget.Others);
        }
    }

    public void ToggleCallingPlayers(bool state)
    {
        if (state)
        {
            IsCalling = true;
            StartCoroutine(CallPlayers());
        }
        else
        {
            IsCalling = false;
        }
    }

    private IEnumerator CallPlayers()
    {
        while (IsCalling)
        {
            fakeAudio.Play();
            yield return new WaitForSeconds(callingInterval);
        }
    }

    [PunRPC]
    public void ForceWin()
    {
        if (wins) return;
        wins = true;
        GameManager.Instance.Win();
    }
}
