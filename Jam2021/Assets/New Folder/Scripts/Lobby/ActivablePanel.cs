using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivablePanel : MonoBehaviourPunCallbacks
{
    public PanelType type;

    public virtual void Activate()
    {
        gameObject.SetActive(true);
    }
    public virtual void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
public enum PanelType
{
    StartScreen,
    ConectionScreen,
    CreateRoomScreen,
    SelectRoom
}