using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConectionPanel : ActivablePanel
{
    [SerializeField] private Button joinButton;
    [SerializeField] private Button CreateRoomButton;
    [SerializeField] private Button backButton;

    private void Start()
    {
        joinButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.SelectRoom);
            ManagePanels.DeactivatePanel(PanelType.ConectionScreen);
        });
        CreateRoomButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.CreateRoomScreen);
            ManagePanels.DeactivatePanel(PanelType.ConectionScreen);
        });
        backButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.StartScreen);
            ManagePanels.DeactivatePanel(PanelType.ConectionScreen);
        });
    }
}
