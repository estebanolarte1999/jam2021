using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoomPanel : ActivablePanel
{
    [SerializeField] private Button backButton;
    [SerializeField] private TMP_InputField roomNameInput;
    [SerializeField] private Button createRoomButton;

    private void Start()
    {
        backButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.ConectionScreen);
            ManagePanels.DeactivatePanel(PanelType.CreateRoomScreen);
        });
        createRoomButton.onClick.AddListener(() =>
        {
            if (roomNameInput.text != "" && !PhotonRoomsManager.PhotonRoomList.ContainsKey(roomNameInput.text))
            {
                PhotonRoomsManager.CreateRoom(roomNameInput.text);
            }
        });
    }
}
