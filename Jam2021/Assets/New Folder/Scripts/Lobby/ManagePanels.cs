using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagePanels : MonoBehaviour
{
    private static ActivablePanel[] panels;
    private void Awake()
    {
        panels = Resources.FindObjectsOfTypeAll(typeof(ActivablePanel)) as ActivablePanel[];
    }
    public static void ActivatePanel(PanelType panel)
    {
        foreach (var item in panels)
        {
            if (item.type == panel)
            {
                item.Activate();
            }
        }
    }
    public static void DeactivatePanel(PanelType panel)
    {
        foreach (var item in panels)
        {
            if (item.type == panel)
            {
                item.Deactivate();
            }
        }
    }
}
