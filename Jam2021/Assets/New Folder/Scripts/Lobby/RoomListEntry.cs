using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomListEntry : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI roomName;
    [SerializeField] private TextMeshProUGUI currentPlayers;
    [SerializeField] private Button joinButton;

    public void Initialize(string _roomName, int _currentPlayers)
    {
        roomName.text = _roomName;
        currentPlayers.text = _currentPlayers + "/2";
        if (_currentPlayers == 2)
        {
            joinButton.interactable = false;
        }
        joinButton.onClick.AddListener(() =>
        {
            PhotonRoomsManager.JoinRoom(roomName.text);
        });
    }
}
