using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectRoomPanel : ActivablePanel
{
    [SerializeField] private Button backButton;
    [SerializeField] private GameObject roomListEntry;
    [SerializeField] private Transform roomsContent;

    private void Start()
    {
        PhotonRoomsManager.OnRoomsUpdated += OnRoomListUpdated;

        backButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.ConectionScreen);
            ManagePanels.DeactivatePanel(PanelType.SelectRoom);
        });
    }
    public override void Activate()
    {
        base.Activate();
        OnRoomListUpdated();
    }
    private void OnRoomListUpdated()
    {
        foreach (var item in roomsContent.GetComponentsInChildren<RoomListEntry>())
        {
            Destroy(item.gameObject);
        }
        foreach (var item in PhotonRoomsManager.PhotonRoomList)
        {
            var newObject = Instantiate(roomListEntry, roomsContent);
            newObject.GetComponent<RoomListEntry>().Initialize(item.Key, item.Value.PlayerCount);
        }
    }
}
