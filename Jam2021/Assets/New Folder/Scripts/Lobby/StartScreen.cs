using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : ActivablePanel
{
    [SerializeField] private Button playButton;

    private void Awake()
    {
        playButton.interactable = false;

        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.ConnectUsingSettings();
    }
    private void Start()
    {
        playButton.onClick.AddListener(() =>
        {
            ManagePanels.ActivatePanel(PanelType.ConectionScreen);
            ManagePanels.DeactivatePanel(PanelType.StartScreen);
        });
    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }
    public override void OnJoinedLobby()
    {
        playButton.interactable = true;    
    }
}
