using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonRoomsManager : MonoBehaviourPunCallbacks
{
    public static Dictionary<string, RoomInfo> PhotonRoomList;
    public static Action OnRoomsUpdated;

    public static PhotonRoomsManager Instance;
    private void Awake()
    {
        Instance = this;

        PhotonRoomList = new Dictionary<string, RoomInfo>();
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (var info in roomList)
        {
            // Remove room from cached room list if it got closed, became invisible or was marked as removed
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (PhotonRoomList.ContainsKey(info.Name))
                {
                    PhotonRoomList.Remove(info.Name);
                }

                continue;
            }

            // Update cached room info
            if (PhotonRoomList.ContainsKey(info.Name))
            {
                PhotonRoomList[info.Name] = info;
            }
            // Add new room info to cache
            else
            {
                PhotonRoomList.Add(info.Name, info);
            }
            OnRoomsUpdated?.Invoke();
        }
    }
    public static void CreateRoom(string roomName)
    {
        RoomOptions roomOptions = new RoomOptions { MaxPlayers = (byte)2};
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    public static void JoinRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }
    public override void OnCreatedRoom()
    {
        PhotonNetwork.LoadLevel("MainGame");
    }
}
