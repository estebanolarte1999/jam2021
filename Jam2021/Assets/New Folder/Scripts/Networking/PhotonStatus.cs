using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PhotonStatus : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI info;
    private void Update()
    {
        info.text = PhotonNetwork.NetworkClientState.ToString();
    }
}
