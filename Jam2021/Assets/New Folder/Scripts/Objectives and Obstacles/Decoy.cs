using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoy : SpawneableObjects
{
    private FakeAudioSource audioS;
    [SerializeField] private float audioInterval = 0.1f;

    private void Awake()
    {
        audioS = GetComponent<FakeAudioSource>();
        if (audioS != null)
            StartCoroutine(ReproduceAudio());
    }

    private IEnumerator ReproduceAudio()
    {
        while (true)
        {
            audioS.Play();
            yield return new WaitForSeconds(audioInterval);
        }
    }
}
