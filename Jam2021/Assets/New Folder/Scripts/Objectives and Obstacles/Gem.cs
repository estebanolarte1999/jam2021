using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Gem : SpawneableObjects, IInteractuable
{
    [SerializeField] private int id = 0;

    private FakeAudioSource audioS;
    [SerializeField] private float audioInterval = 0.1f;

    private string[] sounds = { "ring!", "clink!", "chis!"}; 

    private void Awake()
    {
        audioS = GetComponent<FakeAudioSource>();
        int index = Random.Range(0, sounds.Length);
        audioS.sound = sounds[index]; 
        if (audioS != null)
            StartCoroutine(ReproduceAudio());
    }

    private IEnumerator ReproduceAudio()
    {
        while (true)
        {
            audioS.Play();
            yield return new WaitForSeconds(audioInterval);
        }
    }

    public void SetID(int _id)
    {
        id = _id;
        pv.RPC("SyncIDs", RpcTarget.Others, id);
    }

    public void Interact(PlayerController actor)
    {
        if (!actor.pv.IsMine) return;

        if (actor.ID != id) return;

        PlayerController.Instance.SetGemsSelf(PlayerController.Instance.GetGemsSelf() + 1);

        pv.RPC("SyncScores", RpcTarget.Others, PlayerController.Instance.GetGemsSelf());

        StopAllCoroutines();

        pv.RPC("DisableGameObject", RpcTarget.All);
    }

    [PunRPC]
    public void SyncScores(int gems)
    {
        PlayerController.Instance.SetGemsOther(gems);
    }

    [PunRPC]
    public void SyncIDs(int _id)
    {
        id = _id;
    }
}
