using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : SpawneableObjects
{
    public SpawneableObjects ratObject { get; set; }

    [SerializeField] private Transform ratObjectPivot;

    [SerializeField] private FakeAudioSource audioS;

    [SerializeField] private float stepAudioInterval = 0.4f;

    [SerializeField] private float timeToGrab = 2f;

    [SerializeField] private float timeMoving = 6f;

    [SerializeField] private float timeStill = 3f;

    [SerializeField] private float speed = 2f;

    [SerializeField] private float noiseMagnitude = 2f;

    [SerializeField] private float noiseInterval = 0.3f;

    [SerializeField] private float movementRadius = 5;

    [SerializeField] private Collider2D grabTrigger;

    private Vector2 movementVector;

    private bool moving;

    private Vector3 startPoint;

    private bool allowGrab = true;

    private Animator animator;
    private SpriteRenderer render;

    private void Start()
    {
        StartCoroutine(ReproduceAudio());
        StartCoroutine(Noise());
        StartCoroutine(InternalClock());
        animator = GetComponent<Animator>();
        render = GetComponent<SpriteRenderer>();
    }

    private void FixedUpdate()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        Move();
    }

    public override void Spawn(Vector2 _spawnPoint)
    {
        base.Spawn(_spawnPoint);
        startPoint = transform.position;
    }

    private IEnumerator Noise()
    {
        while (true)
        {
            yield return new WaitForSeconds(noiseInterval);
            if (!PhotonNetwork.IsMasterClient) continue;
            movementVector.x += Random.Range(-noiseMagnitude, noiseMagnitude);
            movementVector.y += Random.Range(-noiseMagnitude, noiseMagnitude);
            movementVector = movementVector.normalized;
        }
    }

    private IEnumerator InternalClock()
    {
        yield return new WaitForSeconds(timeToGrab);
        Destroy(grabTrigger);
        allowGrab = false;

        while (true)
        {
            moving = true;
            OnMove(moving, movementVector);
            yield return new WaitForSeconds(timeMoving);
            moving = false;
            OnMove(moving, movementVector);
            yield return new WaitForSeconds(timeStill);
        }
    }
    private void OnMove(bool _moving, Vector2 dir)
    {
        if (!pv.IsMine) return;
        pv.RPC("OnMouseMoveRpc", RpcTarget.All, _moving, dir.x);
    }
    [PunRPC]
    private void OnMouseMoveRpc(bool _moving, float dir)
    {
        if (animator == null || render == null) return;
        if (dir >0)
        {
            render.flipX = true;
        }
        else if (dir < 0)
        {
            render.flipX = false;
        }
        animator.SetBool("runing", _moving);
    }

    private IEnumerator ReproduceAudio()
    {
        while (true)
        {
            if (moving && (ratObject == null || !ratObject.gameObject.activeSelf))
            {
                audioS.Play();
                yield return new WaitForSeconds(stepAudioInterval);
            }
            else
                yield return new WaitForEndOfFrame();
        }
    }

    private void Move()
    {
        if (!PhotonNetwork.IsMasterClient) return;
        if (!moving) return;
        if (Vector3.Distance(transform.position, startPoint) > movementRadius)
            movementVector = (new Vector2(startPoint.x, startPoint.y) - new Vector2(transform.position.x, transform.position.y)).normalized;
        transform.position += new Vector3(movementVector.x, movementVector.y, 0) * speed * Time.deltaTime;
        OnMove(moving, movementVector);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!allowGrab) return;
        var obj = collision.GetComponent<SpawneableObjects>();
        if (obj == null) return;

        obj.transform.position = ratObjectPivot.position;
        obj.transform.parent = ratObjectPivot;
        ratObject = obj;

        Destroy(grabTrigger);
        allowGrab = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!PhotonNetwork.IsMasterClient) return;
        movementVector *= -1;
    }
}
