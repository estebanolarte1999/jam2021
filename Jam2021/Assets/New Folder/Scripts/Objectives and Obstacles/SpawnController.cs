using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnController : MonoBehaviour
{
    [SerializeField] PhotonView pv;

    [SerializeField] private List<SpawnPoint> spawns;

    [SerializeField] private List<SpawneableObjects> objectivesA;
    [SerializeField] private List<SpawneableObjects> objectivesB;

    [SerializeField] private List<SpawneableObjects> decoys;
    [SerializeField] private List<SpawneableObjects> traps;

    [SerializeField] private SpawneableObjects mouseSpecial;

    [SerializeField] GameManager gameManager;

    private List<int> pvIDsList = new List<int>();

    public void InitLevel(Totem config)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            DeleteAllPreviusElements();
            if (!config.manualSetUp)
                StartCoroutine(SpawnEnviromentElements(config));
        }
    }

    public void DeleteAllPreviusElements()
    {
        foreach (var item in pvIDsList)
        {
            if (PhotonNetwork.IsMasterClient)
                if (PhotonNetwork.GetPhotonView(item) != null)
                    PhotonNetwork.Destroy(PhotonNetwork.GetPhotonView(item));
        }
        pvIDsList.Clear();
    }

    public IEnumerator SpawnEnviromentElements(Totem config)
    {
        pvIDsList.Clear();
        var possibleSpawns = new List<SpawnPoint>();
        var spawnsQueue = new Queue<SpawnPoint>();
        var possibleTraps = new List<SpawneableObjects>();
        var possibleDecoys = new List<SpawneableObjects>();

        foreach (var item in config.spawns)
        {
            if (spawns.Contains(item))
                possibleSpawns.Add(item);
        }
        for (int i = 0; i < possibleSpawns.Count; i++)
        {
            var temp = possibleSpawns[i];
            var r = Random.Range(i, possibleSpawns.Count);
            possibleSpawns[i] = possibleSpawns[r];
            possibleSpawns[r] = temp;
            spawnsQueue.Enqueue(possibleSpawns[i]);
        }

        foreach (var item in config.decoys)
        {
            if (decoys.Contains(item))
                possibleDecoys.Add(item);
        }

        foreach (var item in config.traps)
        {
            if (traps.Contains(item))
                possibleTraps.Add(item);
        }

        var gemsCounterA = config.objectivesPerPlayer;
        var gemsCounterB = config.objectivesPerPlayer;
        var decoyCounter = config.decoysQuantity;
        var trapCounter = config.trapsQuantity;

        yield return new WaitForEndOfFrame();

        for (int i = 0; i < gemsCounterA; i++)
        {
            if (spawnsQueue.Count <= 0) break;

            var r = Random.Range(0, objectivesA.Count);

            var obj = PhotonNetwork.InstantiateRoomObject(objectivesA[r].gameObject.name, Vector3.zero, Quaternion.identity);

            var temp = obj.GetComponent<Gem>();

            temp.SetID(1);
            var spawn = spawnsQueue.Dequeue().Location;
            temp.Spawn(spawn);
            pvIDsList.Add(temp.pv.ViewID);

            var rM = Random.Range(0f, 1f);
            if (rM > config.mouseProb) continue;

            obj = null;
            obj = PhotonNetwork.InstantiateRoomObject(mouseSpecial.gameObject.name, Vector3.zero, Quaternion.identity);
            var tempMouse = obj.GetComponent<Mouse>();
            tempMouse.Spawn(spawn);
            pvIDsList.Add(tempMouse.pv.ViewID);
        }
        for (int i = 0; i < gemsCounterB; i++)
        {
            if (spawnsQueue.Count <= 0) break;

            var r = Random.Range(0, objectivesB.Count);

            var obj = PhotonNetwork.InstantiateRoomObject(objectivesB[r].gameObject.name, Vector3.zero, Quaternion.identity);

            var temp = obj.GetComponent<Gem>();

            temp.SetID(2);
            var spawn = spawnsQueue.Dequeue().Location;
            temp.Spawn(spawn);
            pvIDsList.Add(temp.pv.ViewID);

            var rM = Random.Range(0f, 1f);
            if (rM > config.mouseProb) continue;

            obj = null;
            obj = PhotonNetwork.InstantiateRoomObject(mouseSpecial.gameObject.name, Vector3.zero, Quaternion.identity);
            var tempMouse = obj.GetComponent<Mouse>();
            tempMouse.Spawn(spawn);
            pvIDsList.Add(tempMouse.pv.ViewID);
        }
        for (int i = 0; i < decoyCounter; i++)
        {
            if (spawnsQueue.Count <= 0) break;

            var r = Random.Range(0, possibleDecoys.Count);
            
            var rMoD = Random.Range(0f, 1f);

            GameObject obj = null;

            var spawn = spawnsQueue.Dequeue().Location;

            if (rMoD > config.mouseProb)
            {
                obj = PhotonNetwork.InstantiateRoomObject(possibleDecoys[r].gameObject.name, Vector3.zero, Quaternion.identity);

                var temp = obj.GetComponent<Decoy>();

                temp.Spawn(spawn);
                pvIDsList.Add(temp.pv.ViewID);
                obj = null;
            }

            var rM = Random.Range(0f, 1f);
            if (rM > config.mouseProb) continue;

            obj = PhotonNetwork.InstantiateRoomObject(mouseSpecial.gameObject.name, Vector3.zero, Quaternion.identity);
            var tempMouse = obj.GetComponent<Mouse>();
            tempMouse.Spawn(spawn);
            pvIDsList.Add(tempMouse.pv.ViewID);
        }
        for (int i = 0; i < trapCounter; i++)
        {
            if (spawnsQueue.Count <= 0) break;

            var r = Random.Range(0, possibleTraps.Count);

            var rD = Random.Range(0, possibleDecoys.Count);

            var obj = PhotonNetwork.InstantiateRoomObject(possibleTraps[r].gameObject.name, Vector3.zero, Quaternion.identity);

            var dec = PhotonNetwork.InstantiateRoomObject(possibleDecoys[rD].gameObject.name, Vector3.zero, Quaternion.identity);

            var temp = obj.GetComponent<Trap>();
            var tempD = dec.GetComponent<Decoy>();

            var spawn = spawnsQueue.Dequeue().Location;

            tempD.Spawn(spawn);
            temp.Spawn(spawn);
            pvIDsList.Add(tempD.pv.ViewID);
            pvIDsList.Add(temp.pv.ViewID);
        }
        pv.RPC("SyncIDs", RpcTarget.Others, pvIDsList.ToArray());
    }

    [PunRPC]
    public void SyncIDs(int[] ids)
    {
        pvIDsList.Clear();
        foreach (var item in ids)
        {
            pvIDsList.Add(item);
        }
    }
}

