using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public Vector2 Location { private set; get; }

    private void Awake()
    {
        Location = new Vector2(transform.position.x, transform.position.y);
    }
}
