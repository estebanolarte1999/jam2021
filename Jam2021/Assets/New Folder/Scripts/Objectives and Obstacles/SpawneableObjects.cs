using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public abstract class SpawneableObjects : MonoBehaviour
{
    public PhotonView pv;

    public Vector2 spawnPoint { get; set; }

    private void Awake()
    {
        pv = GetComponent<PhotonView>();
    }
        
    public virtual void Spawn(Vector2 _spawnPoint)
    {
        spawnPoint = _spawnPoint;
        transform.position = new Vector3(spawnPoint.x, spawnPoint.y, 0);
        if(PhotonNetwork.IsMasterClient)
            pv.RPC("SyncSpawns", RpcTarget.Others, _spawnPoint);
    }

    public void Despawn()
    {
        pv.RPC("PunDespawn", RpcTarget.MasterClient);
    }

    [PunRPC]
    public void PunDespawn()
    {
        PhotonNetwork.Destroy(pv);
    }

    [PunRPC]
    public void DisableGameObject()
    {
        gameObject.SetActive(false);
    }

    [PunRPC]
    public void SyncSpawns(Vector2 _spawnPoint)
    {
        Spawn(_spawnPoint);
    }

}
