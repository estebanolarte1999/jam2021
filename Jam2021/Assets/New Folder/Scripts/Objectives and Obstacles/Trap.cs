using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Trap : SpawneableObjects , IInteractuable
{
    public bool ActiveTrap { get; set; }

    private bool trapIsActing = false;

    private PlayerController victim;

    private Animator animator;

    private void Start()
    {
        ActiveTrap = true;
        animator = GetComponentInChildren<Animator>();
    }

    public void Interact(PlayerController actor)
    {
        if (ActiveTrap)
        {
            if (!actor.pv.IsMine) return;
            victim = actor;
            victim.transform.position = transform.position;
            victim.movement.AllowMovement = false;
            victim.movement.AllowMovementInput = false;
            victim.Rooted = true;
            trapIsActing = true;
            ActiveTrap = false;
            pv.RPC("SyncTrapStatus", RpcTarget.Others, ActiveTrap);
            pv.RPC("OnActivateTrap", RpcTarget.All);
        }
        else
        {
            if (!actor.pv.IsMine) return;
            if (actor == victim) return;
            trapIsActing = false;
            pv.RPC("FreeOther", RpcTarget.Others);
            pv.RPC("DisableGameObject", RpcTarget.All);
        }
    }
    [PunRPC]
    public void OnActivateTrap()
    {
        animator.SetTrigger("activate");
    }

    [PunRPC]
    public void SyncTrapStatus(bool status)
    {
        ActiveTrap = status;
    }

    [PunRPC]
    public void FreeOther()
    {
        trapIsActing = false;
        victim.movement.AllowMovement = true;
        victim.movement.AllowMovementInput = true;
        victim.Rooted = false;
    }
}
