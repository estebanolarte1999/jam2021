using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    [SerializeField] private Button backToMenu;
    private IEnumerator Start()
    {
        PhotonNetwork.Disconnect();
        backToMenu.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Lobby");
        });
        yield return new WaitForSeconds(5);
        while (transform.localPosition.y <= 2347f)
        {
            transform.position += new Vector3(0, 1f, 0);

            yield return new WaitForEndOfFrame();
        }
    }
}
