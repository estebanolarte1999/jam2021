using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject tutorialPanel;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private Button continueTutorialButton;
    [SerializeField] private GameObject[] tutorialMessages;

    [SerializeField, Space()] TextMeshProUGUI clock;
    [SerializeField] private TextMeshProUGUI points;
    [SerializeField] private Transform lifesContainerMax;
    [SerializeField] private Transform lifesContainerCurrent;
    [SerializeField] private Transform gemContainerMax;
    [SerializeField] private Transform gemContainerCurrent;

    [SerializeField, Space()] GameObject backgorundLife;
    [SerializeField] private GameObject life;
    [SerializeField] private GameObject backgroundGem;
    [SerializeField] private GameObject gem;

    [SerializeField] private float timerFadeDuration = 5f;

    private float currentFadeTime;

    private int currentTutorialIndex = 0;

    private IEnumerator Start()
    {
        for (int i = 0; i < tutorialMessages.Length; i++)
        {
            tutorialMessages[i].SetActive(i == 0);
        }

        gamePanel.SetActive(false);
        tutorialPanel.SetActive(true);

        continueTutorialButton.onClick.AddListener(
            () =>
            {
                currentTutorialIndex++;
                for (int i = 0; i < tutorialMessages.Length; i++)
                {
                    tutorialMessages[i].SetActive(i == currentTutorialIndex);
                }
                if(currentTutorialIndex >= tutorialMessages.Length)
                {
                    gamePanel.SetActive(true);
                    tutorialPanel.SetActive(false);
                }
            });

        GameManager.Instance.OnLifesChange += OnLifesChange;
        GameManager.Instance.OnMaxGemsChange += OnGemsMaxChange;
        GameManager.Instance.OnPointsChange += OnPointsChange;
        GameManager.Instance.OnTimePasses += OnClockChange;
        yield return new WaitUntil(() => PlayerController.Instance != null);
        PlayerController.Instance.OnSelfGemsChange += OnGemsChange;
    }

    private void FixedUpdate()
    {
        currentFadeTime = Mathf.MoveTowards(currentFadeTime, timerFadeDuration, Time.deltaTime);

        clock.color = new Color(clock.color.r, clock.color.g, clock.color.b, 1 - (currentFadeTime / timerFadeDuration));
    }

    private void OnClockChange(int min, int seg)
    {
        clock.text = $"{min}:{seg}";
        currentFadeTime = 0;
    }

    private void OnLifesChange(int current, int max)
    {
        var elementsMaxToDestoy = lifesContainerMax.GetComponentsInChildren<Transform>();
        foreach (var item in elementsMaxToDestoy)
        {
            if (item != lifesContainerMax)
                Destroy(item.gameObject);
        }

        var elementsCurrentToDestroy = lifesContainerCurrent.GetComponentsInChildren<Transform>();
        foreach (var item in elementsCurrentToDestroy)
        {
            if (item != lifesContainerCurrent)
                Destroy(item.gameObject);
        }

        for (int i = 0; i < current; i++)
        {
            Instantiate(life, lifesContainerCurrent);
        }
        for (int i = 0; i < max; i++)
        {
            Instantiate(backgorundLife, lifesContainerMax);
        }
    }

    private void OnGemsChange(int value)
    {
        var elementsToDestoy = gemContainerCurrent.GetComponentsInChildren<Transform>();
        foreach (var item in elementsToDestoy)
        {
            if (item != gemContainerCurrent)
                Destroy(item.gameObject);
        }
        for (int i = 0; i < value; i++)
        {
            Instantiate(gem, gemContainerCurrent);
        }
    }

    private void OnGemsMaxChange(int value)
    {
        var elementsToDestoy = gemContainerMax.GetComponentsInChildren<Transform>();
        foreach (var item in elementsToDestoy)
        {
            if (item != gemContainerMax)
                Destroy(item.gameObject);
        }
        for (int i = 0; i < value; i++)
        {
            Instantiate(backgroundGem, gemContainerMax);
        }
    }

    private void OnPointsChange(int value)
    {
        points.text = $"X{value}";
    }
}
