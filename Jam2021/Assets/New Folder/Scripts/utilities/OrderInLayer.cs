﻿using System.Collections;
using UnityEngine;

namespace TMT
{
    public class OrderInLayer : MonoBehaviour
    {
        [SerializeField] bool isStatic;
        SpriteRenderer[] sprites;
        int[] orders;

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(0.3f);
            if (isStatic)
            {
                sprites = GetComponentsInChildren<SpriteRenderer>(true);

                for (int i = 0; i < sprites.Length; i++)
                {
                    sprites[i].sortingOrder = (int)(Mathf.Round(-transform.position.y * 10f))  + i;
                }
            }
            Initialize();
        }

        public void Initialize()
        {
            if (isStatic) return;
            sprites = GetComponentsInChildren<SpriteRenderer>(true);
            orders = new int[sprites.Length];
            for (int i = 0; i < sprites.Length; i++)
            {
                orders[i] = sprites[i].sortingOrder;
            }
            StopAllCoroutines();
            StartCoroutine(UpdateLayer());
        }

        private IEnumerator UpdateLayer()
        {
            while (true)
            {
                yield return new WaitForEndOfFrame();

                for (int i = 0; i < sprites.Length; i++)
                {
                    if (sprites[i] == null) continue;
                    sprites[i].sortingOrder = (int)(Mathf.Round(-transform.position.y * 10f))  + orders[i];
                }
            }
        }
    }
}